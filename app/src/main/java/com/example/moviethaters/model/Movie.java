package com.example.moviethaters.model;

import java.io.Serializable;
import java.util.ArrayList;

public class Movie implements Serializable {
    private Integer code;
    private String title;
    private Casting castingShort;
    private Release release;
    private Integer runtime;
    private ArrayList<Genre> genre;
    private Poster poster;
    private Trailer trailer;
    private String trailerEmbed;
    private ArrayList<Object> link;
    private Statistics statistics;


    public Movie(Integer code, String title, Casting castingShort, Release release, Integer runtime, ArrayList<Genre> genre, Poster poster, Trailer trailer, String trailerEmbed, ArrayList<Object> link, Statistics statistics) {
        this.code = code;
        this.title = title;
        this.castingShort = castingShort;
        this.release = release;
        this.runtime = runtime;
        this.genre = genre;
        this.poster = poster;
        this.trailer = trailer;
        this.trailerEmbed = trailerEmbed;
        this.link = link;
        this.statistics = statistics;
    }

    public Integer getCode() {
        return code;
    }

    public String getTitle() {
        return title;
    }

    public Casting getCastingShort() {
        return castingShort;
    }

    public Release getRelease() {
        return release;
    }

    public Integer getRuntime() {
        return runtime;
    }

    public ArrayList<Genre> getGenre() {
        return genre;
    }

    public Poster getPoster() {
        return poster;
    }

    public Trailer getTrailer() {
        return trailer;
    }

    public String getTrailerEmbed() {
        return trailerEmbed;
    }

    public ArrayList<Object> getLink() {
        return link;
    }

    public Statistics getStatistics() {
        return statistics;
    }
}
