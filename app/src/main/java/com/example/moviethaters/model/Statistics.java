package com.example.moviethaters.model;

import java.io.Serializable;

public class Statistics implements Serializable {
    private Float pressRating;
    private Float pressReviewCount;
    private Float userRating;
    private Float userReviewCount;
    private Float userRatingCount;
    private Integer editorialRatingCount;

    public Statistics(Float pressRating, Float pressReviewCount, Float userRating, Float userReviewCount, Float userRatingCount, Integer editorialRatingCount) {
        this.pressRating = pressRating;
        this.pressReviewCount = pressReviewCount;
        this.userRating = userRating;
        this.userReviewCount = userReviewCount;
        this.userRatingCount = userRatingCount;
        this.editorialRatingCount = editorialRatingCount;
    }

    public Float getPressRating() {
        return pressRating;
    }

    public Float getPressReviewCount() {
        return pressReviewCount;
    }

    public Float getUserRating() {
        return userRating;
    }

    public Float getUserReviewCount() {
        return userReviewCount;
    }

    public Float getUserRatingCount() {
        return userRatingCount;
    }

    public Integer getEditorialRatingCount() {
        return editorialRatingCount;
    }
}
