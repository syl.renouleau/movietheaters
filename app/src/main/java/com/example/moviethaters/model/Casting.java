package com.example.moviethaters.model;

import java.io.Serializable;

public class Casting implements Serializable {
    private String directors;
    private String actors;

    public Casting(String directors, String actors) {
        this.directors = directors;
        this.actors = actors;
    }

    public String getDirectors() {
        return directors;
    }

    public String getActors() {
        return actors;
    }
}
