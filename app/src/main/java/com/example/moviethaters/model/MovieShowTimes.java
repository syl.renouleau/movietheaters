package com.example.moviethaters.model;

import java.io.Serializable;
import java.util.ArrayList;

public class MovieShowTimes implements Serializable {
    private String preview;
    private String releaseWeek;
    private Show onShow;
    private Object version;
    private Object screenFormat;
    private String display;
    private ArrayList<Object> scr;


    public MovieShowTimes(String preview, String releaseWeek, Show onShow, Object version, Object screenFormat, String display, ArrayList<Object> scr) {
        this.preview = preview;
        this.releaseWeek = releaseWeek;
        this.onShow = onShow;
        this.version = version;
        this.screenFormat = screenFormat;
        this.display = display;
        this.scr = scr;
    }

    public String getPreview() {
        return preview;
    }

    public String getReleaseWeek() {
        return releaseWeek;
    }

    public Show getOnShow() {
        return onShow;
    }

    public Object getVersion() {
        return version;
    }

    public Object getScreenFormat() {
        return screenFormat;
    }

    public String getDisplay() {
        return display;
    }

    public ArrayList<Object> getScr() {
        return scr;
    }
}
