package com.example.moviethaters.model;

import java.io.Serializable;

public class Trailer implements Serializable {
    private Integer code;
    private String href;

    public Trailer(Integer code, String href) {
        this.code = code;
        this.href = href;
    }

    public Integer getCode() {
        return code;
    }

    public String getHref() {
        return href;
    }
}
