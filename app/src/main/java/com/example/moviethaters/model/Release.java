package com.example.moviethaters.model;

import java.io.Serializable;
import java.util.Date;

public class Release implements Serializable {
    private Date releaseDate;

    public Release(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }
}
