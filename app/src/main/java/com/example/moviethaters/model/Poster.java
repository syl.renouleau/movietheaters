package com.example.moviethaters.model;

import java.io.Serializable;

public class Poster implements Serializable {
    private String path;
    private String href;

    public Poster(String path, String href) {
        this.path = path;
        this.href = href;
    }

    public String getPath() {
        return path;
    }

    public String getHref() {
        return href;
    }
}
