package com.example.moviethaters.model;

import java.io.Serializable;

public class Genre implements Serializable {
    private Integer code;
    private String name;

    public Genre(Integer code, String name) {
        this.code = code;
        this.name = name;
    }

    public Integer getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
