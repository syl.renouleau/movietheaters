package com.example.moviethaters.model;

import java.io.Serializable;

public class Show implements Serializable {
    private Movie movie;

    public Show(Movie movie) {
        this.movie = movie;
    }

    public Movie getMovie() {
        return movie;
    }
}
