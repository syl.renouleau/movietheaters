package com.example.moviethaters.model;

import java.io.Serializable;
import java.util.ArrayList;

public class Cine implements Serializable {
    private Object place;
    private ArrayList<MovieShowTimes> movieShowtimes;

    public Cine(Object place, ArrayList<MovieShowTimes> movieShowtimes) {
        this.place = place;
        this.movieShowtimes = movieShowtimes;
    }

    public Object getPlace() {
        return place;
    }

    public ArrayList<MovieShowTimes> getMovieShowtimes() {
        return movieShowtimes;
    }
}
