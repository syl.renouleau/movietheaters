package com.example.moviethaters.rest;

import com.example.moviethaters.model.Cine;

import retrofit2.Call;
import retrofit2.http.GET;

public interface CineApi {

    /**
     * Call a webservice with dynamic path
     * Exemple :
     * "https://api.myjson.com/bins/31245"
     * base url : https://api.myjson.com/
     * path : bins/{id}
     * where id = 31245
     * So the method will be called using getNews("31235");
     */

    /**
     * Call a webservice with a static path
     */
    @GET("pam/cine.json")
    Call<Cine> getFile();
}
