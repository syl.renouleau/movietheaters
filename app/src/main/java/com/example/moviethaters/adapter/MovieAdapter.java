package com.example.moviethaters.adapter;

import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.moviethaters.R;
import com.example.moviethaters.fragment.MovieDetailFragment;
import com.example.moviethaters.model.Genre;
import com.example.moviethaters.model.MovieShowTimes;
import com.squareup.picasso.Picasso;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.StringJoiner;


import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;


public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.CineViewHolder>{
    private ArrayList<MovieShowTimes> cine;

    //declare interface
    private OnItemClickListener onClick;

    //make interface like this
    public interface OnItemClickListener {
        void onItemClick(MovieShowTimes m);
    }

    public MovieAdapter(OnItemClickListener listener) {
        this.cine = new ArrayList<>();
        this.onClick = listener;
    }

    @NonNull
    @Override
    public MovieAdapter.CineViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_movie, parent, false);
        Log.d("FRAGMENT", "create");
        return new CineViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CineViewHolder holder, int position) {
        MovieShowTimes mv = cine.get(position);
        holder.textMovieTitle.setText(mv.getOnShow().getMovie().getTitle());
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            StringJoiner joiner = new StringJoiner(",");
            for (Genre g : mv.getOnShow().getMovie().getGenre()) {
                joiner.add(g.getName());
            }
            holder.textMovieCategory.setText(joiner.toString());
        }


        int heures = mv.getOnShow().getMovie().getRuntime()/3600;
        int minutes = (mv.getOnShow().getMovie().getRuntime()/60)%60;
        holder.textMovieDuration.setText(heures + "h" + minutes + "min" );

        if(mv.getOnShow().getMovie().getStatistics().getUserRating() != null) {
            holder.ratingUser.setRating(mv.getOnShow().getMovie().getStatistics().getUserRating());
            holder.userReview.setText(String.valueOf(mv.getOnShow().getMovie().getStatistics().getUserRating()).substring(0,3));
        }
        else {
            holder.ratingUser.setRating(0);
            holder.userReview.setText("0");
        }
        if(mv.getOnShow().getMovie().getStatistics().getPressRating() != null) {
            holder.ratingPress.setRating(mv.getOnShow().getMovie().getStatistics().getPressRating());
            holder.pressReview.setText(String.valueOf(mv.getOnShow().getMovie().getStatistics().getPressRating()).substring(0,3));
        }
        else {
            holder.ratingPress.setRating(0);
            holder.pressReview.setText("0");
        }
        Picasso.get().load(mv.getOnShow().getMovie().getPoster().getHref()).into(holder.affiche);
        holder.bind(cine.get(position), this.onClick);
    }

    @Override
    public int getItemCount() {
        return cine.size();
    }

    public void updateCine( ArrayList<MovieShowTimes> cine) {
        this.cine = cine;
        notifyDataSetChanged();
    }

    public void setCine(ArrayList<MovieShowTimes> cine) {
        this.cine = cine;
    }

    public static class CineViewHolder extends RecyclerView.ViewHolder {
        TextView textMovieTitle;
        TextView textMovieDuration;
        TextView textMovieCategory;
        TextView pressReview;
        TextView userReview;
        RatingBar ratingUser;
        RatingBar ratingPress;
        ImageView affiche;


        CineViewHolder(View view) {
            super(view);
            textMovieTitle = view.findViewById(R.id.movie_title);
            textMovieDuration = view.findViewById(R.id.movie_duration);
            textMovieCategory = view.findViewById(R.id.movie_category);
            pressReview = view.findViewById(R.id.pressReview);
            userReview = view.findViewById(R.id.userReview);
            ratingUser = view.findViewById(R.id.ratingBarUser);
            ratingPress = view.findViewById(R.id.ratingBarPress);
            affiche = view.findViewById(R.id.movie_affiche);

        }

        public void bind(final MovieShowTimes item, final OnItemClickListener listener){
            itemView.setOnClickListener(new View.OnClickListener(){

                @Override
                public void onClick(View v) {
                    listener.onItemClick(item);

                }
            });
        }
    }
}
