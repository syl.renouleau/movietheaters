package com.example.moviethaters.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toolbar;

import com.example.moviethaters.R;
import com.example.moviethaters.adapter.MovieAdapter;
import com.example.moviethaters.model.Cine;
import com.example.moviethaters.model.MovieShowTimes;
import com.example.moviethaters.rest.ApiHelper;
import com.google.gson.Gson;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CineFragment extends Fragment {

    private static final String TAG = "MyActivity";

    private RecyclerView recyclerViewCine;
    MovieAdapter movieAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_movies, container, false);


        getActivity().setTitle("Films");

        movieAdapter = new MovieAdapter(new MovieAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(MovieShowTimes m) {
                Fragment movieDetail = new MovieDetailFragment();
                Bundle args = new Bundle();
                args.putSerializable("movie", m);
                movieDetail.setArguments(args);
                getActivity().getSupportFragmentManager().beginTransaction().addToBackStack(null).replace(R.id.fragmentContainer, movieDetail).commit();
            }
        });
        recyclerViewCine = view.findViewById(R.id.recyclerCine);
        recyclerViewCine.setVisibility(View.VISIBLE);
        recyclerViewCine.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        recyclerViewCine.setAdapter(movieAdapter);

        Call<Cine> listMovies = ApiHelper.getInstance().getCineApi().getFile();
        listMovies.enqueue(new Callback<Cine>() {
            @Override
            public void onResponse(Call<Cine> call, Response<Cine> response) {

                if (response.isSuccessful()) {
                    movieAdapter.updateCine(response.body().getMovieShowtimes());

                } else {
                    //TODO handle failure
                }
            }

            @Override
            public void onFailure(Call<Cine> call, Throwable t) {
                Log.e(TAG, "Fail");
            }
        });


        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);

    }
}
