package com.example.moviethaters.fragment;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.VideoView;

import com.example.moviethaters.R;
import com.example.moviethaters.model.Genre;
import com.example.moviethaters.model.MovieShowTimes;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.util.Locale;
import java.util.StringJoiner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

public class MovieDetailFragment extends Fragment {

    private MovieShowTimes mv;
    private ImageView previewVideo;
    private TextView textDuration;
    private TextView realisateur;
    private TextView acteurs;
    private TextView releaseDate;
    private TextView textCategory;
    private RatingBar userReview;
    private RatingBar pressReview;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail_movie, container, false);

        if(savedInstanceState != null)
            this.mv = (MovieShowTimes) savedInstanceState.getSerializable("movie");
        else
            this.mv = (MovieShowTimes) getArguments().getSerializable("movie");

        previewVideo = view.findViewById(R.id.videoView);
        textDuration = view.findViewById(R.id.detail_duration);
        realisateur = view.findViewById(R.id.detail_realisateur);
        acteurs = view.findViewById(R.id.detail_acteurs);
        releaseDate = view.findViewById(R.id.detail_date);
        textCategory = view.findViewById(R.id.detail_category);
        userReview = view.findViewById(R.id.detail_ratingBarUser);
        pressReview = view.findViewById(R.id.detail_ratingBarPress);
        return view;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);

        int heures = mv.getOnShow().getMovie().getRuntime() / 3600;
        int minutes = (mv.getOnShow().getMovie().getRuntime() / 60) % 60;
        textDuration.setText(heures + "h" + minutes + "min");

        realisateur.setText(mv.getOnShow().getMovie().getCastingShort().getDirectors());
        acteurs.setText(mv.getOnShow().getMovie().getCastingShort().getActors());

        Locale lc = new Locale("fr", "FR");
        DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.LONG, lc);

        releaseDate.setText(dateFormat.format(mv.getOnShow().getMovie().getRelease().getReleaseDate()));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            StringJoiner joiner = new StringJoiner(",");
            for (Genre g : mv.getOnShow().getMovie().getGenre()) {
                joiner.add(g.getName());
            }
            textCategory.setText(joiner.toString());
        }
        if (mv.getOnShow().getMovie().getStatistics().getUserRating() != null)
            userReview.setRating(mv.getOnShow().getMovie().getStatistics().getUserRating());
        if (mv.getOnShow().getMovie().getStatistics().getPressRating() != null)
            pressReview.setRating(mv.getOnShow().getMovie().getStatistics().getPressRating());


        if (mv.getOnShow().getMovie().getTrailer() != null) {

            Picasso.get().load("https://i.ytimg.com/vi/" + mv.getOnShow().getMovie().getTrailer().getHref().split("=")[1] + "/hqdefault.jpg").into(previewVideo);
            previewVideo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/results?search_query=" + mv.getOnShow().getMovie().getTitle() + " Bande Annonce VF FilmActu")));
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(mv.getOnShow().getMovie().getTrailer().getHref())));
                }
            });
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                previewVideo.setForeground(getResources().getDrawable(R.drawable.ic_stat_name));
            }
        }
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getActivity().setTitle(mv.getOnShow().getMovie().getTitle());
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("movie", this.mv);
    }
}
